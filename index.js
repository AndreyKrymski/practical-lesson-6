require("dotenv").config();
const express = require("express");
const app = express();
const { v4: uuidv4 } = require("uuid");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const path = require("path");

app.use(express.static("publick"));
app.use(cookieParser());

const knex = require("knex")({
  client: "pg",
  connection: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT || 5432,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
  },
});
//const findUserByUsername = async (username) => {
//  return await knex("users")
//    .select()
//    .where({ username })
//    .limit(1)
//    .then((data) => data[0])
//}

const findUserBySessionId = async (sessionId) => {
  const session = await knex("sessions")
    .select("user_id")
    .where({ session_id: sessionId })
    .limit(1)
    .then((data) => data[0]);

  if (!session) {
    return;
  }

  return knex("users")
    .select()
    .where({ id: session.user_id })
    .limit(1)
    .then((data) => data[0]);
};

const createSession = async (userId) => {
  const sessionId = uuidv4();

  await knex("sessions").insert({
    user_id: userId,
    session_id: sessionId,
  });
  return sessionId;
};

const deleteSession = async (sessionId) => {
  await knex("sessions").where({ session_id: sessionId }).delete();
};

const auth = () => async (req, res, next) => {
  if (!req.cookies["sessionId"]) {
    return next();
  }
  const user = await findUserBySessionId(req.cookies["sessionId"]);
  req.user = user;
  req.sessioId = req.cookies["sessionId"];
  next();
};

app.get("/", auth(), (req, res) => {
  res.sendFile(path.join(__dirname, "publick", "index.html"));
});

// Маршрут для второй страницы (например, '/page2')
app.get("/page2", auth(), (req, res) => {
  res.sendFile(path.join(__dirname, "publick", "page2.html"));
});

app.post("/login", bodyParser.urlencoded({ extended: false }), async (req, res) => {
  const { username, password } = req.body;
  const login = await knex
    .raw(
      `
  SELECT *
  FROM users
  WHERE username = ?
  AND password = crypt(?, password)
`,
      [username, password]
    )
    .then((result) => {
      if (result.rows.length > 0) {
        return result.rows[0];
      } else {
        console.log("Неверный пароль");
      }
    })
    .catch((error) => {
      console.error("Ошибка запроса:", error);
    });

  if (login) {
    const sessionId = await createSession(login.id);

    res.cookie("sessionId", sessionId, { httpOnly: true }).redirect("/page2");
  }
});

app.get("/logout", auth(), async (req, res) => {
  if (!req.user) {
    res.redirect("/");
  }
  await deleteSession(req.cookies["sessionId"]);

  res.clearCookie("sessionId").redirect("/");
});

app.use((err, req, res, next) => {
  res.status(500).send(err.message);
  next();
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Example app listening on  http://localhost:${port}`);
});
