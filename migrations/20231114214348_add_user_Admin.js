/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.up = function (knex) {
  // Создаем пользователя с паролем в хешированном виде
  return knex.raw(`
    INSERT INTO users (username, password)
    VALUES ('admin', crypt('12345', gen_salt('bf', 8)))
  `);
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex("users").where({ username: "admin" }).del();
};
