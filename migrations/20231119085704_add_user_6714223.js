/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.up = function (knex) {
  // Создаем пользователя с паролем в хешированном виде
  return knex.raw(`
    INSERT INTO users (username, password)
    VALUES ('6714223@mail.ru', crypt('aB8n!@SNGn.B@b', gen_salt('bf', 8)))
  `);
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex("users").where({ username: "6714223@mail.ru" }).del();
};
